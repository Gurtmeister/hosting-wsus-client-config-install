#wsus uitvoer
$date = Get-Date -UFormat "%Y-%m-%d"
$Logfile = "Log-$date.txt"
$Computer = $env:COMPUTERNAME
$StartTime = Get-Date -UFormat "%Y-%m-%d"
if(!(Test-Path -Path C:\scripts\logs\$Logfile  )){
   New-Item c:\scripts\logs\$Logfile 
}


[String]$updateServer1 = $Computer
[Boolean]$useSecureConnection = $False
[Int32]$portNumber = 8530
 
# Load .NET assembly
 
[void][reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration")
 
$count = 0
 
# Connect to WSUS Server
 
$updateServer = [Microsoft.UpdateServices.Administration.AdminProxy]::getUpdateServer($updateServer1,$useSecureConnection,$portNumber)
 
write-output "<<<Connected sucessfully "$Startime" >>>" | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
$updatescope = New-Object Microsoft.UpdateServices.Administration.UpdateScope
 
$u=$updateServer.GetUpdates($updatescope )
 
foreach ($u1 in $u )
 
{
 
if ($u1.IsSuperseded -eq 'True')
 
{
 
write-output Decline Update : $u1.Title $StartTime | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
$u1.Decline()
 
$count=$count + 1
 
}
 
}
 
write-output "Total Declined Updates:"$count"" | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
trap
 
{
 
write-output "Error Occurred $Startime" | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
write-output "Exception Message: $Startime" | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
write-output $_.Exception.Message $Startime | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
write-output $_.Exception.StackTrace $Startime | out-file -Encoding utf8 -Append -filepath C:\scripts\logs\$Logfile
 
exit
 
}

 