Function Enable-remote-management { #Onveilig denk ik
	# remoting enabled en partitioning ( gekopieerd van rik script )
	Configure-SMRemoting.exe -Enable
	winrm quickconfig -quiet
	Enable-PSRemoting -Force
	Set-NetFirewallRule -DisplayGroup 'Windows Management Instrumentation (WMI)' -Enabled true -PassThru
	Set-NetFirewallRule -DisplayGroup 'Remote Event Log Management' -Enabled true -PassThru
	netsh advfirewall firewall set rule group="Remote Volume Management" new enable=yes
	cscript.exe c:\Windows\System32\SCregEdit.wsf /AR 0
	Set-NetFirewallRule -DisplayGroup 'Remote Volume Management' -Enabled true -PassThru
	Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP-PUBLIC" -RemoteAddress Any #Nodig om remote beheer vanaf buiten het subnet mogelijk te maken
		if (-not (Get-NetFirewallRule -DisplayName "Allow Ping" -EA Silent)) { 
			New-NetFirewallRule -Name Allow_Ping -DisplayName "Allow Ping"  -Description "Packet Internet Groper ICMPv4" -Protocol ICMPv4 -IcmpType 8 -Enabled True -Profile Any -Action Allow
		} 
		else { 
			write-host "De regel bestaat al" -ForegroundColor Green;  
		}
	Enable-NetFirewallRule -Displaygroup "Windows Firewall Remote Management"
	Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0 
	Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
	Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "UserAuthentication" -Value 1
}

Function Verwijderen-van-onnodige-services {

	If ((Test-Path "$env:windir\explorer.exe") -eq $False ) {
	sc.exe delete DiagTrack
	}
	Else {
	sc.exe delete DiagTrack
	sc.exe delete XblAuthManager
	sc.exe delete XblGameSave
	}
}

Function Partitie-Driveletter { #Deze functie partitioneerd, formateerd en geeft de schijven de juiste letter. D: ( wederom gestolen van rik )
    try{ 
        Set-WmiInstance -InputObject ( Get-WmiObject -Class Win32_volume -Filter "DriveLetter = 'D:'" ) -Arguments @{DriveLetter='Z:'} -ErrorAction Continue 
    }
    catch{
        Write-Host "De D: schijf is al verplaatst"
    }
    Get-Disk |
    Where partitionstyle -eq 'raw' |
    Initialize-Disk -PartitionStyle GPT -PassThru |
    New-Partition -UseMaximumSize |
    Format-Volume -FileSystem NTFS -NewFileSystemLabel "disk" -Confirm:$false 
    $parts = Get-Partition | Sort-Object Size -Descending| Where-Object -FilterScript{$_.DriveLetter -NE "C"}
    $disks = Get-Disk | Sort-Object Size -Descending | Where-Object -FilterScript{$_.Number -Ne"0"}
    $disk_D = $disks[0].number
    $D = $parts[0].DriveLetter + ":"
		if ($D.Contains("D:")){
			Write-Host "De D schijf is succesvol gepartitioneerd" -ForegroundColor Green
    }
		else {
			Write-Host "De D schijf is waarschijnlijk niet juist gepartitioneerd, de schijfletter wordt nu goed gezet." -ForegroundColor Yellow
			$parts = Get-Partition | Sort-Object Size -Descending| Where-Object -FilterScript{$_.DriveLetter -NE "C"} #Dit command en het onder staande maken een lijst met partities en schijven gesorteerd om grootte, de grootste partitie/schijf die niet C: is word D: en de partitie/schijf daarna E:
			$disks = Get-Disk | Sort-Object Size -Descending | Where-Object -FilterScript{$_.Number -Ne"0"}
			$disk_D = $disks[0].number
			$D = $parts[0].PartitionNumber
			Set-Partition -DiskNumber $disk_D -PartitionNumber $D -NewDriveLetter D
			Format_Disk_Check      
    }
}

Function Format-Disk_Check { #Deze functie test of de grootste schijf die niet C: is de letter D: heeft en de schijf die daarna de grootste is de E: schijf is
    $parts = Get-Partition | Sort-Object Size -Descending| Where-Object -FilterScript{$_.DriveLetter -NE "C"}
    $D = $parts[0].DriveLetter + ":"
		if ($D.Contains("D:")){
			Write-Host "De D schijf is succesvol gepartitioneerd" -ForegroundColor Green
		}
		else {
			Write-Host "De D schijf is waarschijnlijk niet juist gepartitioneerd" -ForegroundColor Yellow   
			Get-Partition         
		}
}

Function Win-Activatie { # Wijzigd de product key en probeert dit te activeren
    Write-host "Zorg dat deze server internet toegang heeft voor online activatie " 
    $key = Read-Host "Typ de product key"
    pause
    slmgr /ipk "$key"
    slmgr /ato
    Write-Host("    -Windows gaat nu controlleren of de activatie gelukt is. Dit kan ongeveer 10 seconden duren") -ForegroundColor Yellow
}

# installing WSUS ( gekopieerd van https://xenappblog.com/2017/automate-wsus-on-windows-2016-server-core/ )
Function WSUS-install {
	Install-WindowsFeature -Name Updateservices,UpdateServices-WidDB,UpdateServices-services -IncludeManagementTools
	MKdir C:\scripts
	cp .\Afwijzen_superseded.ps1 C:\scripts\
	cp .\Writelog.ps1 C:\scripts\
	New-Item -Path C: -Name WSUS -ItemType Directory

	CD "C:\Program Files\Update Services\Tools"
	.\wsusutil.exe postinstall CONTENT_DIR=D:\WSUS
	 
	Write-Verbose "Get WSUS Server Object" -Verbose
	$wsus = Get-WSUSServer
	 
	Write-Verbose "Connect to WSUS server configuration" -Verbose
	$wsusConfig = $wsus.GetConfiguration()
	 
	Write-Verbose "Set to download updates from Microsoft Updates" -Verbose
	Set-WsusServerSynchronization -SyncFromMU
	 
	Write-Verbose "Set Update Languages to English and save configuration settings" -Verbose
	$wsusConfig.AllUpdateLanguagesEnabled = $false           
	$wsusConfig.SetEnabledUpdateLanguages("en")
	$wsusConfig.OobeInitialized = $true
	$wsusConfig.Save()
	 
	Write-Verbose "Get WSUS Subscription and perform initial synchronization to get latest categories" -Verbose
	$subscription = $wsus.GetSubscription()
	$subscription.StartSynchronizationForCategoryOnly()
 
		 While ($subscription.GetSynchronizationStatus() -ne 'NotProcessing') {
		 Write-Host "." -NoNewline
		 Start-Sleep -Seconds 5
		 }
 
	Write-Verbose "Sync is Done" -Verbose

	#Deactiveren van alles omdat alles aanstaat tijdens de initiele configuratie
	Write-Verbose "Disable Products all products" -Verbose
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match ".*" } | Set-WsusProduct -Disable
	
	#voorbeelden voor de te kiezen producten ( pak alleen wat je nodig heb ) ( Get-WsusProduct laat zien hoe de producten heten en dan maak je gewoon een nieuwe regel erbij )
	Write-Verbose "Enable Products: Windows Server 2016, Windows Server 2012, Windows Server 2012R2, Microsoft SQL server 2017, .NET framework" -Verbose
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Windows Server 2016" } | Set-WsusProduct
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Windows Server 2012" } | Set-WsusProduct
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Windows Server 2012R2" } | Set-WsusProduct
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Microsoft SQL server 2017" } | Set-WsusProduct
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Microsoft SQL server 2016" } | Set-WsusProduct	
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "ASP.NET web and Data Framework" } | Set-WsusProduct	
	
	Write-Verbose "Disable Language Packs" -Verbose
	Get-WsusServer | Get-WsusProduct | Where-Object -FilterScript { $_.product.title -match "Language Packs" } | Set-WsusProduct -Disable
	 
	Write-Verbose "Configure the Classifications" -Verbose

	# wat voor soort updates je wil ( pak alleen wat nuttig is )
	 Get-WsusClassification | Where-Object {
	 $_.Classification.Title -in (
	 'Critical Updates',
	 'Definition Updates',
	 'Feature Packs',
	 'Security Updates',
	 'Service Packs',
	 'Update Rollups',
	 'Updates')
	 } | Set-WsusClassification
	 
	Write-Verbose "Configure Synchronizations" -Verbose
	$subscription.SynchronizeAutomatically=$False
	$subscription.Save()
	
	#Moet per week op dinsdag en dit kan niet in WSUS zelf 
	#Write-Verbose "Set synchronization scheduled for midnight each night" -Verbose
	#$subscription.SynchronizeAutomaticallyTimeOfDay= (New-TimeSpan -Hours 0)
	#$subscription.NumberOfSynchronizationsPerDay=1
	#$subscription.Save()
	Write-Verbose "Aanmaken van de update groupen Ontwikkel, Test en productie"
	$wsus.CreateComputerTargetGroup("Test")
	$wsus.CreateComputerTargetGroup("Ontwikkel")
	$wsus.CreateComputerTargetGroup("Productie")
	 
	Write-Verbose "Kick Off Synchronization" -Verbose
	$subscription.StartSynchronization()
	 
	Write-Verbose "Monitor Progress of Synchronisation" -Verbose
	 
	Start-Sleep -Seconds 60 # Wait for sync to start before monitoring
		 while ($subscription.GetSynchronizationProgress().ProcessedItems -ne $subscription.GetSynchronizationProgress().TotalItems) {
		 #$subscription.GetSynchronizationProgress().ProcessedItems * 100/($subscription.GetSynchronizationProgress().TotalItems)
		 Start-Sleep -Seconds 5
	}
	Write-Verbose "Installatie is volledig, ga via MMC naar updateservice op het beheerstation en dan $env:COMPUTERNAME met port 8530" -Verbose
 }
 
Function Client-update-task ($length = 20){ 
   $punc = 46..46
    $digits = 48..57
    $letters = 65..90 + 97..122

    # Thanks to
    # https://blogs.technet.com/b/heyscriptingguy/archive/2012/01/07/use-pow
    $Password = get-random -count $length `
        -input ($punc + $digits + $letters) |
            % -begin { $aa = $null } `
            -process {$aa += [char]$_} `
            -end {$aa}
			
			
	$Secure_String_Pwd = ConvertTo-SecureString "$Password" -AsPlainText -Force
	New-LocalUser "taskmaster" -Password $Secure_String_Pwd -FullName "Taskmaster" -Description "task scheduler account."
	Add-LocalGroupMember -Group "Administrators" -Member "taskmaster"
	cmdkey /add:$env:COMPUTERNAME /user:taskmaster /pass:$Password

	
$accproperty = [wmi] "Win32_userAccount.Domain='$env:COMPUTERNAME',Name='taskmaster'"
$SID = $accproperty.SID
$author = "$env:COMPUTERNAME\taskmaster"
[XML]$XMLDOCU = get-content -Path Basetaskweekly.xml
$XMLDOCU.Task.Principals.Principal.UserId = $SID
$XMLDOCU.Task.Principals.Principal.LogonType = "Password"
$XMLDOCU.Task.Principals.Principal.Runlevel = "HighestAvailable"
$XMLDOCU.Task.RegistrationInfo.Author = $author
$XMLDOCU.Task.Triggers.CalendarTrigger.StartBoundary = "2018-10-19T03:00:00"
$DAY = $XMLDOCU.CreateElement("Tuesday")
$XMLDOCU.Task.Triggers.CalendarTrigger.ScheduleByWeek.WeeksInterval = "1"
$XMLDOCU.Task.Triggers.CalendarTrigger.ScheduleByWeek.DaysOfWeek.AppendChild($DAY)
$XMLDOCU.Task.Actions.Exec.Command = 'powershell'
$XMLDOCU.Task.Actions.Exec.Argumnets = "-noexit -command Get-WUInstall –MicrosoftUpdate –AcceptAll –AutoReboot -install | out-file C:\test.txt"
$XMLDOCU.save("$env:TEMP\Update_elke_dinsdag.xml")
schtasks.exe /create /tn Dinsdagsync /XML $env:TEMP\Update_elke_dinsdag.xml /ru $env:COMPUTERNAME\taskmaster /rp $Password
}

Function Server-tasks ($length = 20){ 
   $punc = 46..46
    $digits = 48..57
    $letters = 65..90 + 97..122

    # Thanks to
    # https://blogs.technet.com/b/heyscriptingguy/archive/2012/01/07/use-pow
    $Password = get-random -count $length `
        -input ($punc + $digits + $letters) |
            % -begin { $aa = $null } `
            -process {$aa += [char]$_} `
            -end {$aa}
			
			
	$Secure_String_Pwd = ConvertTo-SecureString "$Password" -AsPlainText -Force
	New-LocalUser "taskmaster" -Password $Secure_String_Pwd -FullName "Taskmaster" -Description "task scheduler account."
	Add-LocalGroupMember -Group "Administrators" -Member "taskmaster"
	cmdkey /add:$env:COMPUTERNAME /user:taskmaster /pass:$Password

	
$accproperty = [wmi] "Win32_userAccount.Domain='$env:COMPUTERNAME',Name='taskmaster'"
$SID = $accproperty.SID
$author = "$env:COMPUTERNAME\taskmaster"
#weekelikjse server taak (purge updates die niet meer van toepassing zijn)
[XML]$XMLDOCU = get-content -Path Basetaskweekly.xml
$XMLDOCU.Task.Principals.Principal.UserId = $SID
$XMLDOCU.Task.Principals.Principal.LogonType = "Password"
$XMLDOCU.Task.Principals.Principal.Runlevel = "HighestAvailable"
$XMLDOCU.Task.RegistrationInfo.Author = $author
$XMLDOCU.Task.Triggers.CalendarTrigger.StartBoundary = "2018-10-19T03:00:00"
$XMLDOCU.Task.Triggers.CalendarTrigger.ScheduleByWeek.WeeksInterval = "1"
$XMLDOCU.Task.Actions.Exec.Command = 'powershell'
$XMLDOCU.Task.Actions.Exec.Argumnets = "-noexit -command Get-WsusServer | Invoke-WsusServerCleanup –CleanupObsoleteUpdates -CleanupUnneededContentFiles -CompressUpdates -DeclineExpiredUpdates -DeclineSupersededUpdates"
$XMLDOCU.save("$env:TEMP\purge_elke_dinsdag.xml")
schtasks.exe /create /tn Dinsdagsync /XML $env:TEMP\purge_elke_dinsdag.xml /ru $env:COMPUTERNAME\taskmaster /rp $Password
#maandlijkse server taak (maandelijkse sync met windows update) eerste dinsdag van de maand
[XML]$XMLDOCU = get-content -Path Basetasmonthly.xml
$XMLDOCU.Task.Principals.Principal.UserId = $SID
$XMLDOCU.Task.Principals.Principal.LogonType = "Password"
$XMLDOCU.Task.Principals.Principal.Runlevel = "HighestAvailable"
$XMLDOCU.Task.Triggers.CalendarTrigger.ScheduleByMonthDayOfWeek.Weeks.Week = "1"
$XMLDOCU.Task.RegistrationInfo.Author = $author
$XMLDOCU.Task.Triggers.CalendarTrigger.StartBoundary = "2018-10-19T05:00:00"
$XMLDOCU.Task.Actions.Exec.Command = 'powershell'
$XMLDOCU.Task.Actions.Exec.Argumnets = "-noexit -command (Get-WsusServer).GetSubscription().StartSynchronization()"
$XMLDOCU.save("$env:TEMP\elke_eerste_dinsdag_sync.xml")
schtasks.exe /create /tn Dinsdagsync /XML $env:TEMP\elke_eerste_dinsdag_sync.xml /ru $env:COMPUTERNAME\taskmaster /rp $Password
#maandelijkse server taak declinen van superseded updates
[XML]$XMLDOCU = get-content -Path Basetasmonthly.xml
$XMLDOCU.Task.Principals.Principal.UserId = $SID
$XMLDOCU.Task.Principals.Principal.LogonType = "Password"
$XMLDOCU.Task.Principals.Principal.Runlevel = "HighestAvailable"
$XMLDOCU.Task.Triggers.CalendarTrigger.ScheduleByMonthDayOfWeek.Weeks.Week = "2"
$XMLDOCU.Task.RegistrationInfo.Author = $author
$XMLDOCU.Task.Triggers.CalendarTrigger.StartBoundary = "2018-10-19T03:00:00"
$XMLDOCU.Task.Actions.Exec.Command = 'powershell'
$XMLDOCU.Task.Actions.Exec.Argumnets = "-ExecutionPolicy Bypass C:\scripts\Afwijzen_superseded.ps1"
$XMLDOCU.save("$env:TEMP\elke_tweede_dinsdag_decline.xml")
schtasks.exe /create /tn Dinsdagsync /XML $env:TEMP\elke_tweede_dinsdag_decline.xml /ru $env:COMPUTERNAME\taskmaster /rp $Password
}

Function Client-config-wsus-module-import {
$dvdpath = (get-volume).Where({$_.DriveType -eq 'CD-ROM'}).DriveLetter
Copy-Item -Path "$dvdpath:policyfileeditor\*" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\policyfileeditor\" -Recurse -Force;
Copy-Item -Path "$dvdpath:PSWindowsUpdate\*" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\PSWindowsUpdate\" -Recurse -Force;
Import-Module policyfileeditor
Import-Module PSWindowsUpdate
}

Function Client-config-wsus {
[string]$wsusserver = Read-Host 'Wat is de url van WSUS-server bijv. http://10.0.0.1:8530 of http://core01:8530'
$MachineDir = "$env:windir\system32\GroupPolicy\Machine\registry.pol"
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName '**del.FillEmptyContentUrls' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -valueName 'AcceptTrustedPublisherCerts' -Data '1' -Type 'DWord'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -valueName 'AllowAutoWindowsUpdateDownloadOverMeteredNetwork' -Data '0' -Type 'DWord'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName 'DoNotConnectToWindowsUpdateInternetLocations' -Data '1' -Type 'DWord'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName 'ExcludeWUDriversInQualityUpdate' -Data '1' -Type 'DWord' -Verbose
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName 'UpdateServiceUrlAlternate' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName 'WUServer' -Data $wsusserver -Type 'String'    
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate' -ValueName 'WUStatusServer' -Data $wsusserver -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.AllowMUUpdateService' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.AUOptions' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.AutomaticMaintenanceEnabled' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallDay' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallEveryWeek' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallFirstWeek' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallFourthWeek' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallThirdWeek' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName '**del.ScheduledInstallTime' -Data '' -Type 'String'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName 'NoAutoUpdate' -Data '1' -Type 'DWord'
Set-PolicyFileEntry -Path $MachineDir -Key 'Software\Policies\Microsoft\Windows\WindowsUpdate\AU' -ValueName 'UseWUServer' -Data '1' -Type 'DWord'
}

$menu = @"
1 Aanzetten van remote management, Telemerty uitzetten en Partitioneren van de D:\ schijf.
2 WSUS installeren (hiervoor zijn twee schijven nodig een voor os andere voor updates D:\ wordt update repo.
3 Windows activeren.
4 Clients configureren met tasks voor in de scheduler en registry keys voor het wijzen naar WSUS en het deactiveren van automatische updates.
5 Sconfig ( Het wiel is al uitgevonden ) voor ip, dns en computernaam instellen.
Q Quit
Select a task by number or Q to quit
"@

Write-Host "Het Wsus server instaleer menu" -ForegroundColor Cyan
$r = Read-Host $menu
Switch ($r) {
"1" {
	Write-Host "Telemetry uitzetten, remote management aanzetten, partitioneren van de WSUS schijven" -ForegroundColor Green
    Verwijderen-van-onnodige-services
	Enable_Remote_Management
	Partitie_Driveletter
}

"2" {
    Write-Host "WSUS installeren" -ForegroundColor Green
    WSUS-install
}

"3" {
    Write-Host "Windows activatie" -ForegroundColor Green
    Win_Activatie
}

"4" {
    Write-Host "Taken exporteren en locale gpo's invullen zodat de clients naar WSUS geredirect worden" -ForegroundColor Green
    Client-config-wsus-module-import
	Client-update-task
	Client-config-wsus
} 

"5" {
    Write-Host "Sconfig" -ForegroundColor Green
    sconfig
}

"Q" {
    Write-Host "Quitting" -ForegroundColor Green
}
 
default {
    Write-Host "I don't understand what you want to do." -ForegroundColor Yellow
}
} #end switch